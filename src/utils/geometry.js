/* @flow */
export const distance = ({ dx, dy }: { dx: number, dy: number }) => Math.sqrt(dx ** 2 + dy ** 2);

export const direction = ({
  x1 = 0,
  x2 = 0,
  y1 = 0,
  y2 = 0,
  dx = x2 - x1,
  dy = y2 - y1
}: {
  x1?: number,
  x2?: number,
  y1?: number,
  y2?: number,
  dx?: number,
  dy?: number
}) => {
  if (dx === 0) {
    if (dy > 0) {
      return 90;
    }
    if (dy < 0) {
      return 270;
    }
    return undefined;
  }

  const quadrant = (Math.atan(Math.abs(dy / dx)) * 180) / Math.PI;

  if (dx > 0) {
    if (dy > 0) {
      return quadrant;
    }
    return 360 - quadrant;
  }
  if (dy > 0) {
    return 180 - quadrant;
  }
  return 180 + quadrant;
};
