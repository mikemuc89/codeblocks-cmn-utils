import { createUrlFromIdAndParams } from '../location';

const PATHS_CONFIG = {
  '/COMPONENTS': {
    props: {
      config: {
        childViews: [
          '/COMPONENTS/WITH_REQ_PARAM',
          '/COMPONENTS/WITH_OPT_PARAM',
          '/COMPONENTS/WITH_MANY_REQ_PARAMS',
          '/COMPONENTS/WITH_MANY_OPT_PARAMS',
          '/COMPONENTS/WITH_MIXED_PARAMS'
        ],
        parent: null,
        url: '/components'
      }
    }
  },
  '/COMPONENTS/WITH_REQ_PARAM': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-req-param/:param'
      }
    }
  },
  '/COMPONENTS/WITH_OPT_PARAM': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-opt-param/(:param)'
      }
    }
  },
  '/COMPONENTS/WITH_MANY_REQ_PARAMS': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-many-req-params/:param1/:param2/:param3'
      }
    }
  },
  '/COMPONENTS/WITH_MANY_OPT_PARAMS': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-many-opt-params/(:param1)/(:param2)/(:param3)'
      }
    }
  },
  '/COMPONENTS/WITH_MIXED_PARAMS': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-opt-param/(:param)'
      }
    }
  },
  '/COMPONENTS/NESTED': {
    props: {
      config: {
        childViews: [
          '/COMPONENTS/NESTED/LEV2',
          '/COMPONENTS/NESTED/LEV2_WITH_PARAM',
          '/COMPONENTS/NESTED/LEV2_WITH_PARAM_OPT'
        ],
        parent: '/COMPONENTS',
        url: '/components/nested'
      }
    }
  },
  '/COMPONENTS/NESTED/LEV2': {
    props: {
      config: {
        childViews: ['/COMPONENTS/NESTED'],
        parent: '/COMPONENTS/NESTED',
        url: '/components/nested/lev2'
      }
    }
  },
  '/COMPONENTS/NESTED/LEV2_WITH_PARAM': {
    props: {
      config: {
        childViews: ['/COMPONENTS/NESTED'],
        parent: '/COMPONENTS/NESTED',
        url: '/components/nested/lev2-with-param/:id'
      }
    }
  },
  '/COMPONENTS/NESTED/LEV2_WITH_PARAM_OPT': {
    props: {
      config: {
        childViews: ['/COMPONENTS/NESTED'],
        parent: '/COMPONENTS/NESTED',
        url: '/components/nested/lev2-with-param/(:idOpt)'
      }
    }
  },
  '/FORMATTERS': {
    props: {
      config: { childViews: [], parent: null, url: '/formatters/:someParam' }
    }
  },
  '/GENERATORS': {
    props: {
      config: {
        childViews: [],
        parent: null,
        url: '/generators/(:someOptParam)'
      }
    }
  },
  '/HANDLERS': {
    props: {
      config: {
        childViews: ['/HANDLERS/SOME_HANDLER'],
        parent: null,
        url: '/handlers'
      }
    }
  },
  '/HANDLERS/SOME_HANDLER': {
    props: {
      config: {
        childViews: [],
        parent: '/HANDLERS',
        url: '/handlers/some-handler'
      }
    }
  }
};

const TEST_PATHS = [
  {
    entry: { id: '/NOT_EXISTS', params: {} },
    expected: null
  },
  {
    entry: { id: '/COMPONENTS', params: {} },
    expected: '/components'
  },
  {
    entry: { id: '/COMPONENTS', params: { requestParam: 'abc' } },
    expected: '/components?requestParam=abc'
  },
  {
    entry: { id: '/COMPONENTS/WITH_REQ_PARAM', params: { param: 'required' } },
    expected: '/components/with-req-param/required'
  },
  {
    entry: {
      id: '/COMPONENTS/WITH_REQ_PARAM',
      params: { param: 'required', additional: 'test' }
    },
    expected: '/components/with-req-param/required?additional=test'
  },
  {
    entry: {
      id: '/COMPONENTS/WITH_REQ_PARAM',
      params: { missingRequired: 'test' }
    },
    expected: TEST_THROW
  }
];

test('utils.location:createUrlFromIdAndParams', () => {
  TEST_PATHS.forEach(({ entry, expected }) => {
    const { id, params } = entry;
    if (expected === TEST_THROW) {
      expect(() => {
        createUrlFromIdAndParams(id, params, PATHS_CONFIG);
      }).toThrow();
    } else {
      const url = createUrlFromIdAndParams(id, params, PATHS_CONFIG);
      expect(url).toBe(expected);
    }
  });
});
