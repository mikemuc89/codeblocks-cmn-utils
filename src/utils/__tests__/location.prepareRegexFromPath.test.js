import { prepareRegexFromPath } from '../location';

const param = '(/[\\w\\d]+)';
const suffix = '/?';

const TEST_PATHS = {
  '': { path: `${suffix}`, required: [], optional: [] },
  '/': { path: `${suffix}`, required: [], optional: [] },
  '/components': { path: `/components${suffix}`, required: [], optional: [] },
  '/components/': { path: `/components${suffix}`, required: [], optional: [] },
  '/components/badge': {
    path: `/components/badge${suffix}`,
    required: [],
    optional: []
  },
  '/components/button/': {
    path: `/components/button${suffix}`,
    required: [],
    optional: []
  },
  '/components/tab/:id': {
    path: `/components/tab${param}${suffix}`,
    required: ['id'],
    optional: []
  },
  '/components/step/:id/': {
    path: `/components/step${param}${suffix}`,
    required: ['id'],
    optional: []
  },
  '/components/select/:id/:asd': {
    path: `/components/select${param}${param}${suffix}`,
    required: ['id', 'asd'],
    optional: []
  },
  '/components/icon/:id/:asd/': {
    path: `/components/icon${param}${param}${suffix}`,
    required: ['id', 'asd'],
    optional: []
  },
  '/components/checkbox/(:id)': {
    path: `/components/checkbox${param}?${suffix}`,
    required: [],
    optional: ['id']
  },
  '/components/checkbox-group/(:id)/': {
    path: `/components/checkbox-group${param}?${suffix}`,
    required: [],
    optional: ['id']
  },
  '/components/checklist/(:id)/(:asd)': {
    path: `/components/checklist${param}?${param}?${suffix}`,
    required: [],
    optional: ['id', 'asd']
  },
  '/components/radio/(:id)/(:asd)/': {
    path: `/components/radio${param}?${param}?${suffix}`,
    required: [],
    optional: ['id', 'asd']
  },
  '/components/radio-group/:id/(:asd)': {
    path: `/components/radio-group${param}${param}?${suffix}`,
    required: ['id'],
    optional: ['asd']
  },
  '/components/warning/:id/(:asd)/': {
    path: `/components/warning${param}${param}?${suffix}`,
    required: ['id'],
    optional: ['asd']
  },
  '/components/info/:id/:abc/(:asd)': {
    path: `/components/info${param}${param}${param}?${suffix}`,
    required: ['id', 'abc'],
    optional: ['asd']
  },
  '/components/code/:id/:abc/(:asd)/': {
    path: `/components/code${param}${param}${param}?${suffix}`,
    required: ['id', 'abc'],
    optional: ['asd']
  },
  '/components/toc/:id/(:asd)/(:abc)': {
    path: `/components/toc${param}${param}?${param}?${suffix}`,
    required: ['id'],
    optional: ['asd', 'abc']
  },
  '/components/topic/:id/(:asd)/(:abc)/': {
    path: `/components/topic${param}${param}?${param}?${suffix}`,
    required: ['id'],
    optional: ['asd', 'abc']
  },
  '/components/input/:id/:abc/(:asd)/(:kkk)': {
    path: `/components/input${param}${param}${param}?${param}?${suffix}`,
    required: ['id', 'abc'],
    optional: ['asd', 'kkk']
  },
  '/components/paragraph/:id/:abc/(:asd)/(:kkk)/': {
    path: `/components/paragraph${param}${param}${param}?${param}?${suffix}`,
    required: ['id', 'abc'],
    optional: ['asd', 'kkk']
  }
};

test('utils.location:prepareRegexFromPath', () => {
  Object.entries(TEST_PATHS).forEach(([key, expected]) => {
    if (expected === TEST_THROW) {
      expect(() => {
        prepareRegexFromPath(key);
      }).toThrow();
    } else {
      const { path, required, optional } = prepareRegexFromPath(key);
      expect(path).toBe(expected.path);
      expect(required).toEqual(expected.required);
      expect(optional).toEqual(expected.optional);
    }
  });
});
