import { getNavigationParamsFromLocation } from '../location';

const PATHS_CONFIG = {
  '/COMPONENTS': {
    props: {
      config: {
        childViews: [
          '/COMPONENTS/WITH_REQ_PARAM',
          '/COMPONENTS/WITH_OPT_PARAM',
          '/COMPONENTS/WITH_MANY_REQ_PARAMS',
          '/COMPONENTS/WITH_MANY_OPT_PARAMS',
          '/COMPONENTS/WITH_MIXED_PARAMS'
        ],
        parent: null,
        url: '/components'
      }
    }
  },
  '/COMPONENTS/WITH_REQ_PARAM': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-req-param/:param'
      }
    }
  },
  '/COMPONENTS/WITH_OPT_PARAM': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-opt-param/(:param)'
      }
    }
  },
  '/COMPONENTS/WITH_MANY_REQ_PARAMS': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-many-req-params/:param1/:param2/:param3'
      }
    }
  },
  '/COMPONENTS/WITH_MANY_OPT_PARAMS': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-many-opt-params/(:param1)/(:param2)/(:param3)'
      }
    }
  },
  '/COMPONENTS/WITH_MIXED_PARAMS': {
    props: {
      config: {
        childViews: [],
        parent: '/COMPONENTS',
        url: '/components/with-opt-param/(:param)'
      }
    }
  },
  '/COMPONENTS/NESTED': {
    props: {
      config: {
        childViews: [
          '/COMPONENTS/NESTED/LEV2',
          '/COMPONENTS/NESTED/LEV2_WITH_PARAM',
          '/COMPONENTS/NESTED/LEV2_WITH_PARAM_OPT'
        ],
        parent: '/COMPONENTS',
        url: '/components/nested'
      }
    }
  },
  '/COMPONENTS/NESTED/LEV2': {
    props: {
      config: {
        childViews: ['/COMPONENTS/NESTED'],
        parent: '/COMPONENTS/NESTED',
        url: '/components/nested/lev2'
      }
    }
  },
  '/COMPONENTS/NESTED/LEV2_WITH_PARAM': {
    props: {
      config: {
        childViews: ['/COMPONENTS/NESTED'],
        parent: '/COMPONENTS/NESTED',
        url: '/components/nested/lev2-with-param/:id'
      }
    }
  },
  '/COMPONENTS/NESTED/LEV2_WITH_PARAM_OPT': {
    props: {
      config: {
        childViews: ['/COMPONENTS/NESTED'],
        parent: '/COMPONENTS/NESTED',
        url: '/components/nested/lev2-with-param/(:idOpt)'
      }
    }
  },
  '/FORMATTERS': {
    props: {
      config: { childViews: [], parent: null, url: '/formatters/:someParam' }
    }
  },
  '/GENERATORS': {
    props: {
      config: {
        childViews: [],
        parent: null,
        url: '/generators/(:someOptParam)'
      }
    }
  },
  '/HANDLERS': {
    props: {
      config: {
        childViews: ['/HANDLERS/SOME_HANDLER'],
        parent: null,
        url: '/handlers'
      }
    }
  },
  '/HANDLERS/SOME_HANDLER': {
    props: {
      config: {
        childViews: [],
        parent: '/HANDLERS',
        url: '/handlers/some-handler'
      }
    }
  }
};

const TEST_PATHS = {
  '/components': { id: '/COMPONENTS', pathParams: {}, requestParams: {} },
  '/components?reqParam1=1&reqParam2=2': {
    id: '/COMPONENTS',
    pathParams: {},
    requestParams: { reqParam1: '1', reqParam2: '2' }
  },
  '/components/?reqParam1=1&reqParam2=2': {
    id: '/COMPONENTS',
    pathParams: {},
    requestParams: { reqParam1: '1', reqParam2: '2' }
  },
  '/components/with-req-param/testParam': {
    id: '/COMPONENTS/WITH_REQ_PARAM',
    pathParams: { param: 'testParam' },
    requestParams: {}
  },
  '/components/with-many-req-params/reqParam1/reqParam2/reqParam3/': {
    id: '/COMPONENTS/WITH_MANY_REQ_PARAMS',
    pathParams: {
      param1: 'reqParam1',
      param2: 'reqParam2',
      param3: 'reqParam3'
    },
    requestParams: {}
  },
  '/components/with-many-req-params/reqParam1/reqParam2/reqParam3': {
    id: '/COMPONENTS/WITH_MANY_REQ_PARAMS',
    pathParams: {
      param1: 'reqParam1',
      param2: 'reqParam2',
      param3: 'reqParam3'
    },
    requestParams: {}
  }
};

test('utils.location:getNavigationParamsFromLocation', () => {
  Object.entries(TEST_PATHS).forEach(([key, expected]) => {
    const [pathname, search] = key.split('?');
    if (expected === TEST_THROW) {
      expect(() => {
        getNavigationParamsFromLocation({ pathname, search }, PATHS_CONFIG);
      }).toThrow();
    } else {
      const { id, pathParams, requestParams } = getNavigationParamsFromLocation({ pathname, search }, PATHS_CONFIG);
      expect(id).toBe(expected.id);
      expect(pathParams).toEqual(expected.pathParams);
      expect(requestParams).toEqual(expected.requestParams);
    }
  });
});
