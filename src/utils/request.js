/* @flow */
import axios from 'axios';

export default (url: string, meta?: Object) =>
  new Promise((resolve: () => Object) => {
    axios.post(url, meta).then(
      ({ data, status }: { data: Object, status: string }) => resolve({ ...data, status }),
      (errors: typeof Error) => {
        console.error(errors.toString());
        if (errors && errors.config && errors.statusText) {
          resolve({ errors: errors.config.statusText });
        } else {
          resolve({
            errors: {
              errors: 'Application error',
              exception: errors.toString()
            }
          });
        }
      }
    );
  });
