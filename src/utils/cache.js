/* @flow */
export const generateCacheKey = (value: any) => {
  if (value instanceof File) {
    const { lastModified, name, size, type } = value;
    return JSON.stringify({ lastModified, name, size, type });
  }
};
