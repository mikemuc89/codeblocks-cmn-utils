/* @flow */
import { type LocationType } from '@omnibly/codeblocks-cmn-types';

type UrlParamsType = {|
  optionalParams: Array<string>,
  regexUrl: typeof RegExp,
  requiredParams: Array<string>
|};

const PATH_PARAMS_GROUP_RE = /\([^)]+\)/gi;
const REQ_PARAMS_LEADING_QMARK_RE = /^\?(.*)/;
const PARAMS_REGEX_RE_REQUIRED = /\/(:([^/)]+))/g;
const PARAMS_REGEX_RE_REQUIRED_REPLACE = '(/[\\w\\d]+)';
const PARAMS_REGEX_RE_OPTIONAL = /\/(\(:([^/)]+)\))/g;
const PARAMS_REGEX_RE_OPTIONAL_REPLACE = '(/[\\w\\d]+)?';

export const DIALOG_DELIMITER = '#@';
export const OPTIONAL_PATH_ENDING = '/?';
export const OPTIONAL_PATH_ENDING_REGEX = /\/\?\s*$/;
export const PATH_SEPARATOR = '/';
export const ROOT_PATH = PATH_SEPARATOR;

export const prepareRegexFromPath = (path: string) => {
  if (!path) {
    return {};
  }
  const clean = (value: string): string => value.replace(/\/\(?:([^)]+)\)?/g, '$1');
  const regexUrl =
    path
      .replace(PARAMS_REGEX_RE_REQUIRED, PARAMS_REGEX_RE_REQUIRED_REPLACE)
      .replace(PARAMS_REGEX_RE_OPTIONAL, PARAMS_REGEX_RE_OPTIONAL_REPLACE)
      .replace(/\/*$/, OPTIONAL_PATH_ENDING) || OPTIONAL_PATH_ENDING;
  const requiredParams = (path.match(PARAMS_REGEX_RE_REQUIRED) || []).map(clean);
  const optionalParams = (path.match(PARAMS_REGEX_RE_OPTIONAL) || []).map(clean);

  return { optionalParams, regexUrl, requiredParams };
};

export const getRequestParamsFromLocation = (location: LocationType) =>
  location.search
    ? location.search
        .replace(REQ_PARAMS_LEADING_QMARK_RE, '$1')
        .split('&')
        .reduce((result: Object, param: string) => {
          const [key, value] = param.split('=');
          return {
            ...result,
            [key.toLowerCase()]: value
          };
        }, {})
    : {};

export const getPathParamsFromLocation = (
  location: LocationType,
  { optionalParams, regexUrl, requiredParams }: UrlParamsType
) => {
  const params = (new RegExp(`^${regexUrl}$`, 'gi').exec(location.pathname) || []).slice(1);
  if (params.length < requiredParams.length) {
    const error = `Not enough required parameters in location. Location: { pathname: ${location.pathname}, search: ${
      location.search
    } }, RequiredParams: { ${requiredParams.join(', ')} }`;
    console.error(error);
    throw error;
  }
  const getParam = (paramsArray: Array<string>) =>
    ((param?: string) => param && param.replace(/^\//, ''))(paramsArray.shift());
  return {
    ...requiredParams.reduce(
      (result: Object, key: string) => ({
        ...result,
        [key]: getParam(params)
      }),
      {}
    ),
    ...optionalParams.reduce(
      (result: Object, key: string) => ({
        ...result,
        [key]: getParam(params)
      }),
      {}
    )
  };
};

export const getNavigationParamsFromLocation = (location: LocationType, pathConfig: Object) => {
  const { pathname } = location;
  const foundConfig = Object.keys(pathConfig).reduce((result?: Object, id: string) => {
    if (result) {
      return result;
    }
    const { regexUrl, requiredParams, optionalParams } = prepareRegexFromPath(pathConfig[id].url);
    if (regexUrl && new RegExp(`^${regexUrl}$`, 'i').test(pathname)) {
      return { id, optionalParams, regexUrl, requiredParams };
    }
    return null;
  }, null);
  if (!foundConfig) {
    const error = `Path config based on location not found. Location: { pathname: ${location.pathname}, search: ${location.search} }`;
    console.error(error);
    throw error;
  }
  const { id } = foundConfig;
  const pathParams = getPathParamsFromLocation(location, foundConfig);
  const requestParams = getRequestParamsFromLocation(location);
  return { id, pathParams, requestParams };
};

export const getParamsFromLink = (link: string): { id: string, params: Object } => {
  const [id, search] = link.split(':');
  return {
    id,
    params: getRequestParamsFromLocation({ pathname: '', search })
  };
};

export const createUrlFromIdAndParams = (id: string, { data }: Object, pathConfig: Object): string => {
  const config = pathConfig[id];
  if (!config) {
    if (typeof window === 'undefined') {
      return id;
    }
    const error = `No config for given id: ${id}`;
    console.error(error);
    return '#';
  }
  const { regexUrl, requiredParams, optionalParams } = prepareRegexFromPath(config.url);

  if (!regexUrl) {
    return ROOT_PATH;
  }

  const matches = regexUrl.match(PATH_PARAMS_GROUP_RE) || [];

  const paramsCopy = { ...data };
  const requiredPathPart = requiredParams.reduce((result: string, key: string, idx: number) => {
    const param = paramsCopy[key];
    if (!param) {
      const error = `Missing required parameter in params for given id: ${id}`;
      console.error(error);
      throw error;
    }
    delete paramsCopy[key];
    return result.replace(matches[idx], `/${param}`);
  }, regexUrl.replace(OPTIONAL_PATH_ENDING_REGEX, ''));
  const optionalPathPart = optionalParams.reduce((result: string, key: string, idx: number) => {
    const param = paramsCopy[key] || '';
    delete paramsCopy[key];
    return result.replace(matches[idx], `/${param}`);
  }, requiredPathPart);
  return (
    optionalPathPart
      .concat(
        Object.keys(paramsCopy).length
          ? `?${Object.entries(paramsCopy)
              .reduce((res: Array<string>, [key, value]: [string, mixed]) => res.concat(`${key}=${value.toString()}`), [])
              .join('&')}`
          : ''
      )
      .replace(OPTIONAL_PATH_ENDING_REGEX, '') || ROOT_PATH
  );
};

export const createAppLinkFromRef = (id: string, { data = {} }: { data: Object } = {}, paths: Object = {}) => {
  if (id in paths) {
    return `#${id.replace(/(.*)\/*$/g, '$1')}:${Object.entries(data)
      .filter(([key, value]: [string, mixed]) => value !== undefined)
      .map(([key, value]: [string, mixed]) => `${key.toUpperCase()}=${value.toString()}`)
      .join('&')}`.replace(/:$/, '');
  }
  return id;
};

export const getNavigationTree = (id: string, pathConfig: Object) => {
  if (!pathConfig[id]) {
    const error = `Invalid id: ${id}`;
    console.error(error);
    throw error;
  }

  const getDefaultContentTree = (id: string, result: Array<Object> = []) => {
    const { defaultContent } = pathConfig[id];
    if (!defaultContent) {
      return result;
    }
    return getDefaultContentTree(defaultContent, [...result, defaultContent]);
  };

  const result = id.split(PATH_SEPARATOR).reduce((result: Array<string>, item: string, index: number, parts: Array<string>) => {
    const newItem = parts.slice(0, index + 1).join(PATH_SEPARATOR);
    const config = pathConfig[newItem];
    if (!item || !config) {
      return result;
    }
    return [...result, newItem, ...(newItem === id ? getDefaultContentTree(newItem) : [])];
  }, []);
  return result;
};
