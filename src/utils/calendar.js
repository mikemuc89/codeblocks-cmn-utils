/* @flow */
import padString from './padString';


type InputDateType = Date | string | null;

export const SECOND = 1000;
export const MINUTE = SECOND * 60;
export const HOUR = MINUTE * 60;
export const DAY = HOUR * 24;
export const WEEK_LENGTH = 7;
const HOUR_SEPARATOR = ':';
const MINUTES_IN_HOUR = 60;

export const dateToTimeString = (value: Date, { seconds }: { seconds?: boolean } = {}) =>
  `${padString(value.getHours(), 2)}:${padString(value.getMinutes(), 2)}${
    seconds ? `:${padString(value.getSeconds(), 2)}` : ''
  }`;

export const numberToTimeString = (value: number) => {
  const hours = Math.floor(value / MINUTES_IN_HOUR);
  const minutes = value - hours * MINUTES_IN_HOUR;
  return `${padString(hours, 2)}${HOUR_SEPARATOR}${padString(minutes, 2)}`;
};

export const getDayDelta = (date: InputDateType, deltaDays: number) => {
  const sanitized = new Date(date);

  return new Date(sanitized.getFullYear(), sanitized.getMonth(), sanitized.getDate() + deltaDays);
};

export const getMonthDelta = (date: InputDateType, deltaMonths: number) => {
  const sanitized = new Date(date);
  const newDate = new Date(sanitized.getFullYear(), sanitized.getMonth() + deltaMonths, sanitized.getDate());
  return newDate.getDate() === sanitized.getDate()
    ? newDate
    : new Date(new Date(newDate.getFullYear(), newDate.getMonth(), 1) - DAY);
};

export const getYearDelta = (date: InputDateType, deltaYears: number) => {
  const sanitized = new Date(date);
  const newDate = new Date(sanitized.getFullYear() + deltaYears, sanitized.getMonth(), sanitized.getDate());
  return newDate.getDate() === sanitized.getDate() ? newDate : new Date(newDate - DAY);
};

export const getWeekDelta = (date: InputDateType, deltaWeeks: number) => {
  const sanitized = new Date(date);
  return new Date(sanitized.getFullYear(), sanitized.getMonth(), sanitized.getDate() + WEEK_LENGTH * deltaWeeks);
};

export const isDateEqual = (date1: Date, date2: Date) =>
  date1 && date2 && date1.toDateString() === date2.toDateString();

export const timeStringToDate = (value: string) => new Date(`1970-01-01T${value}`);

export const timeStringToNumber = (value: string) => {
  try {
    const [hours = 0, minutes = 0] = value.split(HOUR_SEPARATOR);
    return hours * MINUTES_IN_HOUR + minutes;
  } catch {
    return 0;
  }
};
