/* @flow */
export const setElementStyle = (el: HTMLElement, style: Object) => {
  Object.entries(style).forEach(([key, value]) => {
    el.style[key] = value;
  });
};
