/* @flow */
const intToRGB = (value: number) => {
  const c = (value & 0x00ffffff).toString(16);
  return '00000'.substring(0, 6 - c.length) + c;
};

export default (value: string) =>
  intToRGB(value.split('').reduce((result: number, char: string) => char.charCodeAt(0) + ((result << 5) - result), 0));
