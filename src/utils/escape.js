/* @flow */
export default (value: string) => value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
