/* @flow */
const easeInOutQuad = (currentTime: number, beginTime: number, deltaTime: number, duration: number): number => {
  let time = currentTime / (duration / 2);
  if (time < 1) {
    return (deltaTime / 2) * time * time + beginTime;
  }
  time--;
  return (-deltaTime / 2) * (time * (time - 2) - 1) + beginTime;
};

const scrollTo = (element: HTMLElement, to: number, duration?: number = 500) => {
  const start = element.scrollTop;
  const change = to - start;
  let currentTime = 0;
  const increment = 20;

  const animateScroll = (): void => {
    currentTime += increment;
    const val = easeInOutQuad(currentTime, start, change, duration);
    element.scrollTop = val;
    if (currentTime < duration) {
      setTimeout(animateScroll, increment);
    }
  };
  animateScroll();
};

export default (id: string, duration?: number = 500) => {
  const node = document.getElementById(id);
  if (node) {
    const to = node.offsetTop;
    if (document.body) {
      scrollTo(document.body, to, duration);
    }
  }
};
