/* @flow */
export const getCodeFromKeyEvent = (e: KeyboardEvent = window.event) => e.keyCode || e.which;

export const getCharFromKeyEvent = (e: KeyboardEvent = window.event) => {
  const keyCode = getCodeFromKeyEvent(e);
  const charCode = keyCode - 48 * Math.floor(keyCode / 48);
  return String.fromCharCode(keyCode >= 96 ? charCode : keyCode);
};

export const isSpecialCharacter = (e: KeyboardEvent) => {
  const keyCode = getCodeFromKeyEvent(e);
  return (
    keyCode &&
    ([144].includes(keyCode) || (keyCode > 64 && keyCode < 91) || (keyCode > 96 && keyCode < 123) || keyCode <= 47)
  );
};
