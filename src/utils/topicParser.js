/* @flow */
/* eslint-disable quote-props */
/* eslint-disable-next-line max-len */
const RE_TAG =
  /(<(Code|Image|Info|Link|Quote|Section1|Section2|Section3|Section4|Section5|Summary|Warning)([^>]*)(\/>|>([^<]*)<\/\2>))/g;
const RE_TAG_PARAMS = / (\w+)(="([^"]*)")?/g;

const BOOLEAN_MAP = Object.freeze({
  '0': false,
  '1': true,
  false: false,
  true: true
});

const prepareElement = (kind: string, text: string, params: Object = {}) => ({
  kind,
  params: {
    ...params,
    text: text.trim()
  }
});

export default (value: string) => {
  const elements = [];
  let match = RE_TAG.exec(value);

  if (!match && value.trim() !== '') {
    elements.push(prepareElement('Paragraph', value.trim()));
  }

  while (match) {
    const { index, input } = match;
    const tag = match[2].trim();
    const paramsString = match[3].trim();
    const content = match[5].trim();
    const before = input.slice(0, index).trim();
    const [current] = match;
    const after = input.slice(index + current.length).trim();

    if (before !== '') {
      elements.push(prepareElement('Paragraph', before));
    }

    const params = {};
    let paramMatch = RE_TAG_PARAMS.exec(paramsString);

    while (paramMatch) {
      const [_, key, __, value] = paramMatch;
      params[key] = value === undefined ? true : value in BOOLEAN_MAP ? BOOLEAN_MAP[value] : value;
      paramMatch = RE_TAG_PARAMS.exec(paramsString);
    }

    elements.push(prepareElement(tag, content, params));
    match = RE_TAG.exec(after);

    if (!match && after !== '') {
      elements.push(prepareElement('Paragraph', after));
    }
  }

  return elements;
};
