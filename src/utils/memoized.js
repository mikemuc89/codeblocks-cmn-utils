/* @flow */
export default (fn: Function) => {
  const memos = {};
  return (...args: Array<any>) => {
    const key = JSON.stringify(args);
    if (key in memos) {
      return memos[key];
    }
    memos[key] = fn(...args);
    return memos[key];
  };
};
