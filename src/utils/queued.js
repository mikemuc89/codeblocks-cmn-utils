/* @flow */
export default (fn: Function, initialDelay?: number = 0) => {
  const promise = new Promise((resolve: () => any) => {
    setTimeout(() => {
      resolve();
    }, initialDelay);
  });

  return (...args: Array<any>): Promise<any> =>
    new Promise((resolve: (data: any) => any) => {
      promise
        .then(() => fn(...args))
        .then((data: any) => {
          resolve(data);
        });
    });
};
