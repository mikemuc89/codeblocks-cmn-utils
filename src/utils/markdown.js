/* @flow */
import MarkdownIt from 'markdown-it';

const md = new MarkdownIt({
  breaks: true,
  html: true,
  linkify: true,
  xhtmlOut: true
});

export default md;
