/* @flow */
type NumberLike = number | string;

export default ({ max, min, value }: { max: NumberLike, min: NumberLike, value: NumberLike }) => {
  const numMin = parseFloat(min);

  return Math.round((100 * (parseFloat(value) - numMin)) / (parseFloat(max) - numMin));
};
