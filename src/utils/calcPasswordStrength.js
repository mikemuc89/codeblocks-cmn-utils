/* @flow */
const NUMBERS_REGEX = /[0-9]/;
const LOWER_REGEX = /[a-z]/;
const UPPER_REGEX = /[A-Z]/;
const SPECIAL_REGEX = /[^A-Za-z0-9 ]/;

const CHECKS = {
  number: NUMBERS_REGEX,
  lower: LOWER_REGEX,
  upper: UPPER_REGEX,
  special: SPECIAL_REGEX
};

export default (password: string, { desiredLength = 10 }: Object = {}) => Object.entries({
  ...CHECKS,
  length: new RegExp(`.{${desiredLength}}`)
}).reduce(({ checks, score }, [key, regex]) => {
  const check = regex.test(password);
  return {
    checks: {
      ...checks,
      [key]: check
    },
    score: check ? score + 1 : score
  }
}, {
  checks: {},
  score: 0
});
