/* @flow */
import cx from 'classnames';
import { type WithMessagesType } from '@omnibly/codeblocks-cmn-types';
import {
  ALIGN_TYPES,
  KINDS,
  POSITIONS,
  POSITIONS_AROUND,
  POSITIONS_CORNERS,
  POSITIONS_FULL,
  RESULTS,
  SHAPES,
  SIZES
} from '@omnibly/codeblocks-cmn-types/src/enums';

const cxVerticalAlignSingle = (
  styles: Object,
  base: string,
  { verticalAlign }: { verticalAlign?: $Values<typeof VERTICAL_ALIGN_TYPES> },
  ...rest: Array<string>
) =>
  cx(
    verticalAlign &&
      {
        [ALIGN_TYPES.BOTTOM]: styles[`${base}__Bottom`],
        [ALIGN_TYPES.MIDDLE]: styles[`${base}__Middle`],
        [ALIGN_TYPES.TOP]: styles[`${base}__Top`]
      }[verticalAlign],
    ...rest
  );

const cxAlignSingle = (
  styles: Object,
  base: string,
  { align }: { align?: $Values<typeof ALIGN_TYPES> },
  ...rest: Array<string>
) =>
  cx(
    align &&
      {
        [ALIGN_TYPES.CENTER]: styles[`${base}__Center`],
        [ALIGN_TYPES.JUSTIFY]: styles[`${base}__Justify`],
        [ALIGN_TYPES.LEFT]: styles[`${base}__Left`],
        [ALIGN_TYPES.RIGHT]: styles[`${base}__Right`]
      }[align],
    ...rest
  );

const cxClickable = (
  styles: Object,
  base: string,
  {
    clickable,
    onClick
  }: {
    clickable?: boolean,
    onClick?: (e?: Event) => void
  },
  ...rest: Array<string>
) => cx((clickable || onClick) && styles[`${base}__Clickable`], ...rest);

const cxControl = (
  styles: Object,
  base: string,
  {
    active,
    disabled,
    errors,
    focus,
    helper,
    hidden,
    hints,
    required,
    warnings
  }: {
    ...WithMessagesType,
    active?: boolean,
    disabled?: boolean,
    focus?: boolean,
    hidden?: boolean,
    required?: boolean
  },
  ...rest: Array<string>
) =>
  cx(
    active && styles[`${base}__Active`],
    disabled && styles[`${base}__Disabled`],
    focus && styles[`${base}__Focus`],
    errors && styles[`${base}__Error`],
    !errors && warnings && styles[`${base}__Warning`],
    !errors && !warnings && hints && styles[`${base}__Hint`],
    !errors && !warnings && !hints && helper && styles[`${base}__Helper`],
    hidden && styles[`${base}__Hidden`],
    required && styles[`${base}__Required`],
    ...rest
  );

const cxKind = (styles: Object, base: string, { kind }: { kind?: $Values<typeof KINDS> }, ...rest: Array<string>) =>
  cx(
    kind &&
      {
        [KINDS.DARK]: styles[`${base}__Dark`],
        [KINDS.ERROR]: styles[`${base}__Error`],
        [KINDS.INFO]: styles[`${base}__Info`],
        [KINDS.LIGHT]: styles[`${base}__Light`],
        [KINDS.MARKETING]: styles[`${base}__Marketing`],
        [KINDS.PLAIN]: styles[`${base}__Plain`],
        [KINDS.PRIMARY]: styles[`${base}__Primary`],
        [KINDS.SECONDARY]: styles[`${base}__Secondary`],
        [KINDS.SUBMIT]: styles[`${base}__Submit`],
        [KINDS.SUCCESS]: styles[`${base}__Success`],
        [KINDS.WARNING]: styles[`${base}__Warning`]
      }[kind],
    ...rest
  );

const cxOpen = (styles: Object, base: string, { open }: { open?: boolean }, ...rest: Array<string>) =>
  cx(open ? styles[`${base}__Open`] : styles[`${base}__Closed`], ...rest);

const cxOrientation = (
  styles: Object,
  base: string,
  {
    horizontal,
    vertical
  }: {
    horizontal?: boolean,
    vertical?: boolean
  },
  ...rest: Array<string>
) => cx(horizontal && styles[`${base}__Horizontal`], vertical && styles[`${base}__Vertical`], ...rest);

const cxPositionCross = (
  styles: Object,
  base: string,
  { position }: { position: $Values<typeof POSITIONS> },
  ...rest: Array<string>
) =>
  cx(
    position
      ? {
          [POSITIONS.BOTTOM]: styles[`${base}__Bottom`],
          [POSITIONS.LEFT]: styles[`${base}__Left`],
          [POSITIONS.RIGHT]: styles[`${base}__Right`],
          [POSITIONS.TOP]: styles[`${base}__Top`]
        }[position]
      : styles[`${base}__Inline`],
    ...rest
  );

const cxPositionCorners = (
  styles: Object,
  base: string,
  { position }: { position: $Values<typeof POSITIONS_CORNERS> },
  ...rest: Array<string>
) =>
  cx(
    position
      ? {
          [POSITIONS_CORNERS.BOTTOM_LEFT]: styles[`${base}__BottomLeft`],
          [POSITIONS_CORNERS.BOTTOM_RIGHT]: styles[`${base}__BottomRight`],
          [POSITIONS_CORNERS.TOP_LEFT]: styles[`${base}__TopLeft`],
          [POSITIONS_CORNERS.TOP_RIGHT]: styles[`${base}__TopRight`]
        }[position]
      : styles[`${base}__Inline`],
    ...rest
  );

const cxPosition = (
  styles: Object,
  base: string,
  props: { position: $Values<typeof POSITIONS_FULL> },
  ...rest: Array<string>
) => cx(cxPositionCorners(styles, base, props), cxPositionCross(styles, base, props), ...rest);

const cxPositionAround = (
  styles: Object,
  base: string,
  props: { position: $Values<typeof POSITIONS_AROUND> },
  ...rest: Array<string>
) =>
  cx(
    cxPositionCorners(styles, base, props),
    cxPositionCross(styles, base, props),
    {
      [POSITIONS_AROUND.LEFT_BOTTOM]: styles[`${base}__LeftBottom`],
      [POSITIONS_AROUND.LEFT_TOP]: styles[`${base}__LeftTop`],
      [POSITIONS_AROUND.RIGHT_BOTTOM]: styles[`${base}__RightBottom`],
      [POSITIONS_AROUND.RIGHT_TOP]: styles[`${base}__RightTop`]
    }[props.position],
    ...rest
  );

const cxResult = (
  styles: Object,
  base: string,
  { result }: { result: $Values<typeof RESULTS> },
  ...rest: Array<string>
) =>
  cx(
    result &&
      {
        [RESULTS.BUG]: styles[`${base}__Bug`],
        [RESULTS.CONDITIONAL]: styles[`${base}__Conditional`],
        [RESULTS.EMPTY]: styles[`${base}__Empty`],
        [RESULTS.ERROR_404]: styles[`${base}__Error404`],
        [RESULTS.ERROR_500]: styles[`${base}__Error500`],
        [RESULTS.FAILURE]: styles[`${base}__Failure`],
        [RESULTS.MAIL_CONFIRMATION]: styles[`${base}__MailConfirmation`],
        [RESULTS.MODIFIED]: styles[`${base}__Modified`],
        [RESULTS.NOT_ALLOWED]: styles[`${base}__NotAllowed`],
        [RESULTS.PENDING]: styles[`${base}__Pending`],
        [RESULTS.PHONE_CONFIRMATION]: styles[`${base}__PhoneConfirmation`],
        [RESULTS.POWER_OFF]: styles[`${base}__PowerOff`],
        [RESULTS.PRINTED]: styles[`${base}__Printed`],
        [RESULTS.REMOVED]: styles[`${base}__Removed`],
        [RESULTS.SUCCESS]: styles[`${base}__Success`],
        [RESULTS.UNDETERMINED]: styles[`${base}__Undetermined`]
      }[result],
    ...rest
  );

const cxShape = (styles: Object, base: string, { shape }: { shape: $Values<typeof SHAPES> }, ...rest: Array<string>) =>
  cx(
    shape &&
      {
        [SHAPES.CIRCLE]: styles[`${base}__Circle`],
        [SHAPES.ROUNDED]: styles[`${base}__Rounded`],
        [SHAPES.SQUARE]: styles[`${base}__Square`]
      }[shape],
    ...rest
  );

const cxSize = (styles: Object, base: string, { size }: { size: $Values<typeof SIZES> }, ...rest: Array<string>) =>
  cx(
    size &&
      {
        [SIZES.INHERIT]: styles[`${base}__Inherit`],
        [SIZES.L]: styles[`${base}__L`],
        [SIZES.M]: styles[`${base}__M`],
        [SIZES.S]: styles[`${base}__S`],
        [SIZES.XL]: styles[`${base}__XL`],
        [SIZES.XS]: styles[`${base}__XS`]
      }[size],
    ...rest
  );

const cxNumericSize = (styles: Object, base: string, { size }: { size: number | string }, ...rest: Array<string>) =>
  cx(size && styles[`${base}__Size_${size}`], ...rest);

const cxTransparency = (
  styles: Object,
  base: string,
  {
    semiTransparent,
    transparent
  }: {
    semiTransparent?: boolean,
    transparent?: boolean
  },
  ...rest: Array<string>
) => cx(semiTransparent && styles[`${base}__SemiTransparent`], transparent && styles[`${base}__Transparent`], ...rest);

export default Object.assign(cx, {
  alignSingle: cxAlignSingle,
  clickable: cxClickable,
  control: cxControl,
  kind: cxKind,
  numericSize: cxNumericSize,
  open: cxOpen,
  orientation: cxOrientation,
  position: cxPosition,
  positionAround: cxPositionAround,
  positionCorners: cxPositionCorners,
  positionCross: cxPositionCross,
  result: cxResult,
  shape: cxShape,
  size: cxSize,
  transparency: cxTransparency,
  verticalAlignSingle: cxVerticalAlignSingle
});
