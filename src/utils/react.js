/* @flow */
import * as React from 'react';

export const isBasicReactChild = (element: React.Node) => ['string', 'number'].includes(typeof element);

export const isReactFragment = (element: any) => {
  if (element.type) {
    return element.type === React.Fragment;
  }
  return element === React.Fragment;
};

export const reduceJsxToString = (previous: string, current: React.Node): string => `${previous}${innerText(current)}`;

export const innerText = (jsx: any) => {
  if (!jsx || typeof jsx === 'boolean') {
    return '';
  }

  if (typeof jsx === 'number') {
    return jsx.toString();
  }

  if (typeof jsx === 'string') {
    return jsx;
  }

  if (Array.isArray(jsx)) {
    return jsx.reduce((result: string, item: ReactNode) => `${result}${innerText(item)}`, '');
  }

  return innerText(jsx.props?.children);
};

export const processChildren = (children: React.Node, propsFactory: (child: React.Node, type: any) => React.Node, BasicChild?: React.ComponentType<any>) => children &&
  React.Children.map(children, (child: React.Node) => {
    if (!child) {
      return null;
    }
    if (isBasicReactChild(child)) {
      return BasicChild ? (
        <BasicChild>{child}</BasicChild>
      ) : child
    }
    if (isReactFragment(child)) {
      return React.cloneElement(child, {}, processChildren(child.props.children, propsFactory, BasicChild));
    }
    const { _basicChild: NestedBasicChild, _children: childrenPropsFactory, ...newProps } = propsFactory(child, child.type.componentId);
    return React.cloneElement(child, newProps, childrenPropsFactory ? processChildren(child.props.children, propsFactory, NestedBasicChild) : child.props.children);
  })
