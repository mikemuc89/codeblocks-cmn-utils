/* @flow */
import * as React from 'react';
import { SVG_COMPONENTS } from '@omnibly/codeblocks-cmn-types/src/constants';

export type SvgLayerParamsType = {|
  moveX?: number,
  moveY?: number,
  path: string,
  rotate?: number,
  scale?: number,
  scaleX?: number,
  scaleY?: number
|};

export type SvgLayerType = {|
  id: string,
  params: SvgLayerParamsType
|};

export const prepareSvgDefs = (
  layers: Array<SvgLayerType>,
  {
    LAYER_PROPERTIES = {},
    NOT_DEFINIABLE = []
  }: {
    LAYER_PROPERTIES: Object,
    NOT_DEFINIABLE: Array<string>
  } = {}
) => {
  const seen = {};
  return layers
    .map(({ id }: { id: string }, idx: number) => {
      if (id in seen || NOT_DEFINIABLE.includes(id)) {
        return null;
      }

      seen[id] = true;

      return (({ component: Component, id: href, ...defaultParams }: { component: any, id: string }) => (
        <Component
          key={idx}
          id={id}
          href={Component === SVG_COMPONENTS.USE ? `#${href}` : undefined}
          {...defaultParams}
        />
      ))(LAYER_PROPERTIES[id]);
    })
    .filter(Boolean);
};

export const prepareSvgLayerParams = (
  { moveX, moveY, path, rotate, scale, scaleX, scaleY, ...params }: SvgLayerParamsType = {},
  { svgSize = 100 }: Object = {}
) => {
  const halfSize = svgSize / 2;

  return {
    ...params,
    ...(path ? { d: path } : {}),
    ...((moveX || moveY) ? {
      transform: `translate(${parseFloat(moveX) || 0} ${parseFloat(moveY) || 0})`
    } : {}),
    ...(scale
      ? { transform: `scale(${parseFloat(scale)})` }
      : (scaleX || scaleY) ? {
          transform: `scale(${parseFloat(scaleX) || 1} ${parseFloat(scaleY) || 1})`
        } : {}),
    ...(rotate ? { transform: `rotate(${parseFloat(rotate)} ${halfSize} ${halfSize})` } : {})
  };
};
