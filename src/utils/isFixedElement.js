/* @flow */
const isFixedElement = (element: HTMLElement) =>
  element ? getComputedStyle(element).position === 'fixed' || isFixedElement(element.parentElement) : false;

export default isFixedElement;
