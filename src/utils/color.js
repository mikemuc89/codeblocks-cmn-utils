/* @flow */
import { colord, extend, getFormat, random } from 'colord';
import namesPlugin from "colord/plugins/names";

extend([namesPlugin]);

export { getFormat, random };

export default colord;
