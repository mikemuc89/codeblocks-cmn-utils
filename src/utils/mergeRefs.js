/* @flow */
export default (...refs: Array<any>) => {
  const filteredRefs = refs.filter(Boolean);

  if (!filteredRefs.length) {
    return null;
  }

  return (el: any) => {
    filteredRefs.forEach((ref: any) => {
      if (typeof ref === 'function') {
        ref(el);
      } else if (typeof ref === 'object') {
        ref.current = el;
      }
    });
  };
};
