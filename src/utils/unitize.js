/* @flow */
import { HTML_UNITS } from '@omnibly/codeblocks-cmn-types/src/constants';

export default Object.assign(
  (value: string | number | void, unit: string = HTML_UNITS.PIXEL) => {
    if (value === null || value === undefined) {
      return '';
    }
    const strValue = `${value}`;
    return strValue.match(/^-?\d+$/) ? `${strValue}${unit}` : strValue;
  },
  {
    revert: (value: string | number) => (typeof value === 'number' ? value : parseFloat(value.replace(/px$/gi, '')))
  }
);
