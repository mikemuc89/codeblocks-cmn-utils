/* @flow */
export default (subject: string, length: number, character: string = '0') =>
  (new Array(length).join(character) + String(subject)).slice(-length);
