/* @flow */
import { NOTIFICATION_COOKIES, NOTIFICATION_RODO } from '@omnibly/codeblocks-cmn-types/src/ids';

export const setCookie = (name: string, value: any, days: number) => {
  const date = new Date();
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
  document.cookie = `${name}=${value};expires=${date.toUTCString()}`;
};

export const getCookie = (name: string) => {
  const match = typeof document !== 'undefined' && document.cookie.match(new RegExp(`(^| )${name}=([^;]+)`));
  return match ? match[2] : null;
};

export const hasCookie = (name: string) => {
  const match = typeof document !== 'undefined' && document.cookie.match(new RegExp(`(^| )${name}=([^;]+)`));
  return Boolean(match);
};

export const deleteCookie = (name: string) => {
  document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
};

export const setCookieAgreement = () => setCookie(NOTIFICATION_COOKIES, true, 365 * 10);

export const hasCookieAgreement = () => hasCookie(NOTIFICATION_COOKIES);

export const setRodoAgreement = () => setCookie(NOTIFICATION_RODO, true, 365 * 10);

export const hasRodoAgreement = () => hasCookie(NOTIFICATION_RODO);
