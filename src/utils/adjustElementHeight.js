/* @flow */
import unitize from './unitize';

export default (target: HTMLElement) => {
  const borderBoxSizing = window.getComputedStyle(target, null).boxSizing === 'border-box';
  target.style.height = unitize(1);

  if (borderBoxSizing) {
    target.style.height = unitize(target.scrollHeight);
  } else {
    const borderBottom = parseInt(window.getComputedStyle(target, null).borderBottom);
    const borderTop = parseInt(window.getComputedStyle(target, null).borderTop);
    const paddingBottom = parseInt(window.getComputedStyle(target, null).paddingBottom);
    const paddingTop = parseInt(window.getComputedStyle(target, null).paddingTop);
    target.style.height = unitize(target.scrollHeight - borderBottom - borderTop - paddingBottom - paddingTop + 1);
  }
};
