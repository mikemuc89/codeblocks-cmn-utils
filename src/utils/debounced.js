/* @flow */
export default (fn: Function, delay: number) => {
  let tid;
  return function (...args: Array<any>) {
    if (tid) {
      clearTimeout(tid);
    }
    tid = setTimeout(() => {
      fn(...args);
      tid = null;
    }, delay);
  };
};
