/* @flow */
export default (...funcs: Array<Function>) =>
  (arg: any) =>
    funcs
      .slice(0)
      .reverse()
      .reduce((result: any, func: Function) => func(result), arg);
