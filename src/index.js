/* @flow */
export { default as adjustElementHeight } from './utils/adjustElementHeight';
export { default as calcPasswordStrength } from './utils/calcPasswordStrength';
export { default as color, getFormat as getColorFormat, random as randomColor } from './utils/color';
export { default as compose } from './utils/compose';
export {
  deleteCookie,
  getCookie,
  hasCookie,
  hasCookieAgreement,
  hasRodoAgreement,
  setCookie,
  setCookieAgreement,
  setRodoAgreement
} from './utils/cookies';
export { default as cx } from './utils/cx';
export { default as debounced } from './utils/debounced';
export { default as escape } from './utils/escape';
export { direction, distance } from './utils/geometry';
export { default as getPercent } from './utils/getPercent';
export { default as guid } from './utils/guid';
export { default as hexCode } from './utils/hexCode';
export { default as isFixedElement } from './utils/isFixedElement';
export { getCharFromKeyEvent, getCodeFromKeyEvent, isSpecialCharacter } from './utils/keyUtils';
export {
  createAppLinkFromRef,
  createUrlFromIdAndParams,
  getNavigationParamsFromLocation,
  getNavigationTree,
  getParamsFromLink,
  getPathParamsFromLocation,
  getRequestParamsFromLocation,
  prepareRegexFromPath
} from './utils/location';
export { default as md } from './utils/markdown';
export { default as memoized } from './utils/memoized';
export { default as mergeRefs } from './utils/mergeRefs';
export { default as MessageFormat } from './utils/messageFormat';
export { default as padString } from './utils/padString';
export { default as queued } from './utils/queued';
export {
  isBasicReactChild,
  innerText as reactInnerText,
  processChildren,
  reduceJsxToString
} from './utils/react';
export { default as request } from './utils/request';
export { default as scrollTo } from './utils/scrollTo';
export { setElementStyle } from './utils/styles';
export { default as topicParser } from './utils/topicParser';
export { default as unitize } from './utils/unitize';
export { default as update } from './utils/update';

export const repeatPattern = (times: number, func: Function) =>
  Array.from(Array(times)).map((item: any, index: number) => func(index));

export const stopPropagationCallback = (e: MouseEvent) => e.stopPropagation();
